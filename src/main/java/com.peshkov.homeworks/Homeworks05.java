package com.peshkov.homeworks;

public class Homeworks05 {


    public static void main(String[] args) {
        int[] array = new int[4];

        array[0] = 335;
        array[1] = 256;
        array[2] = 438;
        array[3] = -1;


        int min = 9;

        for (int counter : array) {
            if (counter == -1) break;


            int y = findMinDigit(counter);
            if (y < min) min = y;
        }
    }

    //335
    public static int findMinDigit(int counter) {
        // 5
        // 3
        // 3
        int minDigit = counter % 10;
        // 335
        // 33
        // 3
        // 0 -> цикл закончился
        while (counter != 0) {
            // 5
            // 3
            // 3
            int currentDigit = counter % 10;

            if (currentDigit < minDigit) minDigit = currentDigit;
            // 33
            // 3
            // 0

            counter = counter / 10;
        }
        return minDigit;


    }
}




